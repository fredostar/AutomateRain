#include <Servo.h>

#include "DHT.h"

#define DHTPIN A0     // what pin we're connected to

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT dht(DHTPIN, DHTTYPE);

Servo servo1; 

void setup() {
      
  
    Serial.begin(9600); 
    Serial.println("Initialisation Arrosage !!");

    
    pinMode(4,OUTPUT);
    servo1.attach(4);
    dht.begin();
}

void loop() {
  Serial.print("humidite: ");
  Serial.print(getDonneeCapteurHumidite());
  Serial.println(" %");
  if(seuilArrosageAtteint())
  {
    // active pompe
    demarreServo();
  }
  else
  {
    // desactive pompe
    arreteServo();
    
  }

}


float getDonneeCapteurHumidite()
{
      // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    //float t = dht.readTemperature();
    float h = dht.readHumidity();
    return h;
  
}

bool seuilArrosageAtteint()
{
   if(getDonneeCapteurHumidite() >= 80)
   {
     return  true;
   }
   
   return false;
  
}

void demarreServo()
{
  
  static int v = 0;

  if ( Serial.available()) {
    char ch = Serial.read();

    switch(ch) {
      case '0'...'9':
        v = v * 10 + ch - '0';
        break;
      case 's':
        servo1.write(v);
        v = 0;
        break;
    }
  }
  
  
}

void arreteServo()
{
   static int v = 0;
   
   servo1.write(v);
    v = 0;
 // servo1.detach();
   
}



